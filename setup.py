#!/usr/bin/env python

from setuptools import setup

setup(
    name="nose-notificationcenter",
    version="0.1",
    packages=['nose_notificationcenter'],
    package_data={'nose_notificationcenter': ['images/*']},

    author="Eric Allen",
    author_email="eric.allen@adroll.com",
    description="Nose plugin for sending test results to OS X Notification Center",
    license="MIT",

    entry_points={
        'nose.plugins.0.10': [
            'notificationcenter=nose_notificationcenter:NoseNotificationCenter'
            ]
        }
)
