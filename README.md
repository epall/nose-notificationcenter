# Nose + Notification Center

Send test results from "nosetests" to the OS X Notification Center.

## Installation

    brew install terminal-notifier
    python setup.py install

## Usage

Once installed, you should have a `--with-notificationcenter` option in nosetests globally.

Best used with [nose-watch](https://github.com/lukaszb/nose-watch) for immediate testing feedback.

## Credits

Thanks to Visual Pharm for the success and failure icons, from the "Must Have" collection.

https://www.iconfinder.com/iconsets/musthave

Developed by Eric Allen <eric.allen@adroll.com> while working at AdRoll, Inc.
