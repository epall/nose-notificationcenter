import hashlib
import logging
import os
from subprocess import check_call

from nose.plugins import Plugin


log = logging.getLogger('nose.plugins.notificationcenter')

IMAGE_PATH = os.path.join(os.path.dirname(__file__), 'images')


class NoseNotificationCenter(Plugin):
    name = 'notificationcenter'

    def options(self, parser, env=os.environ):
        super(NoseNotificationCenter, self).options(parser, env=env)

    def configure(self, options, conf):
        super(NoseNotificationCenter, self).configure(options, conf)
        if not self.enabled:
            return

    def help(self):
        return "Send results of tests to OS X Notification Center"

    def finalize(self, result):
        group_id = hashlib.sha256(''.join(result.config.testNames)).hexdigest()

        if result.errors or result.failures:
            check_call(['terminal-notifier', '-message', u'Tests Failed', '-title', 'nosetests', '-activate',
                        'com.apple.Terminal', '-appIcon', os.path.join(IMAGE_PATH, 'failure.png'), '-group', group_id])
        else:
            check_call(['terminal-notifier', '-message', u'Tests Passed', '-title', 'nosetests', '-activate',
                        'com.apple.Terminal', '-appIcon', os.path.join(IMAGE_PATH, 'success.png'), '-group', group_id])
